import { FC } from 'react'
import IndicatorArrow from './Arrow'
import { css } from '@emotion/react'

const RandomIndicator: FC = () => {
  return (
    <>
      <div className={`absolute -top-[16px] left-1/2 -translate-x-1/2`}>
        <IndicatorArrow />
      </div>

      <div
        className={`indicator absolute left-1/2 top-1/2 w-[2px] -translate-x-1/2 -translate-y-1/2 bg-red-500`}
        css={css({
          height: 'calc(100% + 17px)',
        })}
      ></div>

      <div
        className={`absolute -bottom-[16px] left-1/2 -translate-x-1/2 rotate-180`}
      >
        <IndicatorArrow />
      </div>
    </>
  )
}

export default RandomIndicator
