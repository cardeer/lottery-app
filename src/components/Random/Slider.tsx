import { FC } from 'react'
import RandomItem from './Item'
import { css } from '@emotion/react'
import RandomIndicator from './Indicator/Indicator'
import { Interpolation, animated } from '@react-spring/web'

interface IRandomSliderProps {
  items: string[]
  itemSize?: number
  scrollX: Interpolation<string, any>
}

const RandomSlider: FC<IRandomSliderProps> = ({
  items,
  itemSize = 150,
  scrollX,
}) => {
  return (
    <div
      className={`relative`}
      css={css({
        height: `${itemSize}px`,
      })}
    >
      <div className={`h-full w-full overflow-hidden`}>
        <animated.div
          className='slider flex flex-nowrap'
          style={{
            transform: scrollX,
          }}
        >
          {items.map((item, i) => (
            <RandomItem size={itemSize} key={item + i}>
              {item}
            </RandomItem>
          ))}
        </animated.div>
      </div>

      <RandomIndicator />
    </div>
  )
}

export default RandomSlider
