import { FC, useEffect, useState } from 'react'
import Web3 from 'web3'
import { useNavigate } from 'react-router-dom'
import metaMaskLogo from '../../assets/images/metamask.png'
import { useWeb3Store } from '../../store/web3'

import style from './style.module.scss'

const LoginPage: FC = () => {
  const navigate = useNavigate()

  const [error, setError] = useState<string>('')
  const [ready, setReady] = useState<boolean>(false)

  const account = useWeb3Store((state) => state.account)
  const setAccount = useWeb3Store((state) => state.setAccount)

  useEffect(() => {
    function handleAccountChanged(_accounts: unknown) {
      const accounts = _accounts as string[]

      if (accounts.length > 0) {
        setAccount(accounts[0])
        setError('')
      } else {
        setAccount('')
      }
    }

    if (!window.ethereum) {
      setError('Please install MetaMask!')
      return
    } else {
      window.ethereum
        .request<string[]>({ method: 'eth_accounts' })
        .then((accounts: Partial<string[]> | null | undefined) => {
          handleAccountChanged(accounts as string[])
          setReady(true)
        })
    }

    window.ethereum.on('accountsChanged', handleAccountChanged)

    return () => {
      if (window.ethereum) {
        window.ethereum.removeListener('accountsChanged', handleAccountChanged)
      }
    }
  }, [])

  const handleLoginClick = async () => {
    if (!window.ethereum) return

    try {
      await window.ethereum.request({
        method: 'eth_requestAccounts',
      })
      setError('')
    } catch (error: any) {
      if (error.code === 4001) {
        setError(`Cloudn't connect to MetaMask. Please try again.`)
      } else {
        setError(`Something went wrong. Please try again.`)
      }

      return
    }
  }

  const handleContinueClick = () => {
    navigate('/lottery')
  }

  if (!ready) {
    return <></>
  }

  return (
    <div
      className={`flex h-screen flex-col items-center justify-center p-[16px]`}
    >
      <div
        className={
          style['metamask-logo'] +
          ' mb-[50px] rounded-full object-contain p-[3px]'
        }
      >
        <div className={`relative h-full w-full`}>
          <div
            className={`absolute left-0 top-0 h-full w-full rounded-full bg-gray-900`}
          ></div>

          <img
            src={metaMaskLogo}
            className={`pointer-events-none relative max-w-[250px] bg-gray-900 bg-transparent`}
          />
        </div>
      </div>

      {!account ? (
        <button
          className={`rounded-[8px] bg-orange-600 px-[16px] py-[8px] text-[18px] font-bold`}
          onClick={handleLoginClick}
        >
          Login with MetaMask
        </button>
      ) : (
        <button
          className={`rounded-[8px] bg-green-600 px-[16px] py-[8px] text-[18px] font-bold`}
          onClick={handleContinueClick}
        >
          Continue
        </button>
      )}

      {account && (
        <div className={`mt-[24px] text-center`}>
          <div>You can change your account from the MetaMask</div>
          <div className={`mt-[8px]`}>
            Currently connected to{' '}
            <span className={`font-bold text-orange-400 underline`}>
              {account}
            </span>
          </div>
        </div>
      )}

      {error && <div className={`mt-[24px] text-red-500`}>{error}</div>}
    </div>
  )
}

export default LoginPage
