import { FC, useEffect, useState } from 'react'
import { useWeb3Store } from '../../store/web3'
import { useNavigate } from 'react-router-dom'
import { abi, contractAddress } from '../../constants/contract'
import Web3 from 'web3'
import Contract from 'web3-eth-contract'
import RandomSlider from '../../components/Random/Slider'
import { to, useSpring } from '@react-spring/web'

const itemSize = 150

const LotteryPage: FC = () => {
  const navigate = useNavigate()

  const [web3, setWeb3] = useState<Web3>()
  const [contract, setContract] = useState<Contract>()
  const [players, setPlayers] = useState<string[]>([])
  const [isManager, setIsManager] = useState<boolean>(false)
  const [winner, setWinner] = useState<string>('')

  // fake players
  const [items, setItems] = useState<string[]>([])

  const minItems = 100 + (Math.floor(window.innerWidth / itemSize) + 1)

  const [interpolation, interpolationApi] = useSpring(() => ({
    x: 0,
    config: {
      frequency: 1.5,
      bounce: 0.9,
      damping: 0.5,
    },
  }))

  const account = useWeb3Store((state) => state.account)
  const setAccount = useWeb3Store((state) => state.setAccount)

  useEffect(() => {
    function handleAccountsChanged(_accounts: unknown) {
      const accounts = _accounts as string[]
      setAccount(accounts[0])

      if (accounts.length === 0) {
        setAccount('')
        navigate('/')
      } else {
        setAccount(accounts[0])
      }
    }

    if (!window.ethereum) {
      navigate('/')
    } else {
      const web3 = new Web3(Web3.givenProvider)
      setWeb3(web3)

      const contract = new web3!.eth.Contract(abi, contractAddress)
      setContract(contract)

      window.ethereum
        .request({ method: 'eth_accounts' })
        .then(handleAccountsChanged)
      window.ethereum.on('accountsChanged', handleAccountsChanged)

      contract.methods
        .getPlayers()
        .call()
        .then((players: string[]) => {
          setPlayers(players)
          setFakePlayers(players)
        })

      return () => {
        if (window.ethereum) {
          window.ethereum.removeListener(
            'accountsChanged',
            handleAccountsChanged
          )
        }
      }
    }

    return () => {
      if (window.ethereum) {
        window.ethereum.removeListener('accountsChanged', handleAccountsChanged)
      }
    }
  }, [])

  useEffect(() => {
    if (contract && account) {
      ;(async () => {
        const manager = await contract.methods.manager().call()

        if (manager.toLowerCase() === account.toLowerCase()) {
          setIsManager(true)
        } else {
          setIsManager(false)
        }
      })()
    }
  }, [account, contract])

  const setFakePlayers = async (players: string[] = []) => {
    if (players.length === 0) {
      setItems([])
      return
    }

    const tmp = players.slice()
    if (tmp.length < minItems) {
      const addAmount = minItems - tmp.length
      for (let i = 0; i < addAmount; i++) {
        tmp.push(players[Math.floor(Math.random() * players.length)])
      }
      setItems(tmp)
    } else {
      setItems(players)
    }
  }

  const handlePickWinner = async () => {
    if (players.length === 0) return

    await contract?.methods.pickWinner().send({
      from: account,
      gas: '1000000',
    })

    const winner: string = await contract?.methods.winner().call()

    const tmp = items.slice()
    tmp[100] = winner
    setItems(tmp)

    const centerX = window.innerWidth / 2
    const endX = 100.5 * itemSize - centerX

    interpolationApi.start({
      x: endX,
      onRest: async () => {
        setWinner(winner)

        await new Promise((resolve) => setTimeout(resolve, 2000))

        const joinedPlayers: string[] = await contract?.methods
          .getPlayers()
          .call()
        setPlayers(joinedPlayers)
        setFakePlayers(joinedPlayers)

        interpolationApi.stop()
        interpolationApi.set({
          x: 0,
        })
      },
    })
  }

  const handleJoin = async () => {
    setWinner('')

    await contract?.methods.enter().send({
      from: account,
      value: Web3.utils.toWei('0.01', 'ether'),
      gas: '1000000',
    })

    const players: string[] = await contract?.methods.getPlayers().call()
    setPlayers(players)
    setFakePlayers(players)
  }

  if (!account) {
    return <></>
  }

  return (
    <div className={`flex min-h-screen flex-col`}>
      <div className={`px-[16px] pt-[24px] text-center`}>
        Account:{' '}
        <span className={`font-bold text-orange-400 underline`}>{account}</span>
      </div>

      <div className={`my-auto`}>
        <div className={`border-b border-t border-gray-500 py-[8px]`}>
          <RandomSlider
            scrollX={to(interpolation.x, (val) => `translateX(-${val}px)`)}
            items={items}
          />
        </div>

        <div className={`mt-[50px] px-[16px] pb-[24px] text-center`}>
          <div
            className={`mt-[24px] text-center text-[18px] ${
              isManager ? 'text-orange-500' : 'text-white'
            }`}
          >
            {isManager ? 'You are a Manager' : 'Your are a Player'}
          </div>

          <div className={`mt-[16px] flex justify-center gap-[8px]`}>
            {isManager && (
              <button
                className={`rounded-[4px] bg-green-500 px-[16px] py-[8px]`}
                onClick={handlePickWinner}
              >
                Pick a Winner
              </button>
            )}

            <button
              className={`rounded-[4px] bg-orange-500 px-[16px] py-[8px]`}
              onClick={handleJoin}
            >
              Join Now
            </button>
          </div>

          <div className={`mt-[16px]`}>Players Joined: {players.length}</div>
          {winner && (
            <div className={`mt-[24px]`}>
              Congratulations, The winner is{' '}
              <span className={`font-bold text-orange-500 underline`}>
                {winner}
              </span>
            </div>
          )}
        </div>
      </div>
    </div>
  )
}

export default LotteryPage
