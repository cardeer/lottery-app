import { css } from '@emotion/react'
import { FC } from 'react'

const IndicatorArrow: FC = () => {
  return (
    <div
      css={css({
        width: 10,
        height: 10,
        clipPath: `polygon(0 0, 50% 100%, 100% 0)`,
      })}
      className={`bg-red-500`}
    ></div>
  )
}

export default IndicatorArrow
