# Lottery App

## Setup
Run the following command.

Using yarn
```
yarn
```

Using npm
```
npm i
```

Before running the application, you have to change the contract address first.
1. Open .env file
2. Change the value of VITE_CONTRACT_ADDRESS

## Running the Application

Using yarn
```
yarn dev
```

Using npm
```
npm run dev
```