import { FC, LazyExoticComponent, Suspense } from 'react'

interface IRouteElementProps {
  Element: LazyExoticComponent<FC>
}

const RouteElement: FC<IRouteElementProps> = ({ Element }) => {
  return (
    <Suspense fallback={null}>
      <Element />
    </Suspense>
  )
}

export default RouteElement
