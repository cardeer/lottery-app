import MetaMaskSDK from '@metamask/sdk'

declare global {
  interface Window {
    ethereum: MetaMaskSDK.MetaMaskInpageProvider
  }
}

export {}
