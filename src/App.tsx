import { lazy } from 'react'
import { Route, Routes } from 'react-router-dom'
import RouteElement from './components/RouteElement'

const LoginPage = lazy(() => import('./pages/login'))
const LotteryPage = lazy(() => import('./pages/lottery'))

function App() {
  return (
    <>
      <Routes>
        <Route path='/' element={<RouteElement Element={LoginPage} />} />
        <Route
          path='/lottery'
          element={<RouteElement Element={LotteryPage} />}
        />
      </Routes>
    </>
  )
}

export default App
