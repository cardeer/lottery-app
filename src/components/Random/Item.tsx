import { css } from '@emotion/react'
import { FC } from 'react'

interface IRandomItemProps {
  size: number
  children?: React.ReactNode
}

const RandomItem: FC<IRandomItemProps> = ({ size, children }) => {
  return (
    <div
      className={`flex items-center justify-center overflow-hidden break-all border bg-gray-700 p-[8px] text-center text-[14px] text-gray-300`}
      css={css({
        minWidth: `${size}px`,
        minHeight: `${size}px`,
        maxWidth: `${size}px`,
        maxHeight: `${size}px`,
      })}
    >
      {children}
    </div>
  )
}

export default RandomItem
