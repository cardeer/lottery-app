import { create } from 'zustand'

interface IWeb3Store {
  account: string
  setAccount: (account: string) => void
}

export const useWeb3Store = create<IWeb3Store>((set) => ({
  account: '',
  setAccount: (account: string) => set(() => ({ account })),
}))
